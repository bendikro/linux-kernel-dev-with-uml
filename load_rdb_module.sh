#!/bin/bash
#set -x
TFRC=${TFRC:-1}
LOAD=${LOAD:-1}

if [[ "help" == "$1" ]]
    then
    echo "Valid variables:"
	echo "TFRC: $TFRC"
	echo "LOAD: $LOAD"
	exit
fi

modprobe -r tcp_rdb

if [ $LOAD -eq 1 ]
	then
	modprobe tcp_rdb use_tfrc=$TFRC
fi
