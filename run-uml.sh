#!/usr/bin/expect

# This script starts the UML process with the arguments defined in uml_base.sh

source /root/uml/uml_base.sh

send "\n\n\n\n\n\n\n\n"

set QUIT_CHAR ""

set timeout -1

puts "UMLDIR: $UMLDIR\n"
puts "UMLNAME1: $UMLNAME\n"

#	conconsole=ttyS0,115200

set run_pid [spawn $LINUX root=/dev/root rootflags=$UMLROOT rw \
	rootfstype=hostfs \
	umid=$UMLNAME \
	mem=$MEMORY \
	eth0=vde,$UMLVDE0_MAC \
	eth1=vde,$UMLVDE1_MAC \
	con=pts con0=fd:0,fd:1 \
	con1=tty:/dev/tty3 \
	ssl0=pts \
	vga=ask \
	stderr=1 \
	]

#	con1=tty:/dev/tty3 \


puts "UML PID: $run_pid\n"


proc shell {} {
    global spawn_id, user_spawn_id # These are set when spawn is called
    global QUIT_CHAR

	set time_out 1
	while {1} {
		puts "\[Timeout is: $time_out\]"
		interact {
			timeout $time_out { puts "\[tick [tick]\]" }
			+ { incr time_out; return }
			-- - { incr time_out -1; return }
			-o eof {
				puts "We're done here"
				break
			}
		}
	}
	return;

    expect {
            ##
            # gracefully exit when the process exits.
            ##
        eof {
			send "EOF"
#            return
        }

		busy {puts busy\n ; exp_continue}

            ##
            # Catch all from gdb and pass it to the user
            ##
        -re ".+" {
			send "process input";
            send_user -raw -- $expect_out(buffer);
			exp_continue;
        }

            ##
            # Catch all from user and pass it to process
            ##
        -i $user_spawn_id -re .+ {
			send "user input";
            send -- $expect_out(buffer);
			exp_continue;
        }

            ##
            # Catch user QUIT sequence.
            # Currently should be a single character.
            ##
	    -i $user_spawn_id $QUIT_CHAR {return}
    }
}

interact

puts "\r----- run finished----\r"
