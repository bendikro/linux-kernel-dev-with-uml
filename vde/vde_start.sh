#!/usr/bin/expect

source /root/uml/uml_base.sh

# This script creates the vde switch which is passed to the uml in th eth0 argument.
# The switch is connected to the host interface with the command vde_pcapplug.

# Set these to ensure the host forwards packets from uml instance
puts [exec sysctl -w net.ipv4.conf.eth1.proxy_arp=1]
puts [exec sysctl -w net.ipv4.ip_forward=1]
puts [exec sysctl -w net.ipv4.conf.eth0.proxy_arp=1]

set pid1 [spawn vde_switch -F -sock $UMLVDE1 ]
set pid2 [spawn vde_pcapplug -sock $UMLVDE1 $HOST_INTERFACE ]

set pid3 [spawn vde_switch -F -sock $UMLVDE0 ]
set pid4 [spawn vde_pcapplug -sock $UMLVDE0 $HOST_INTERFACE_EXT ]

puts "pid1: $pid1"
puts "pid2: $pid2"
puts "pid3: $pid3"
puts "pid4: $pid4"

interact
puts "\r----- run finished----\r"
