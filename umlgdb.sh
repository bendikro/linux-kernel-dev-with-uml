#!/usr/bin/expect

#######################################################################
# Name: umlgdl
#
# Description:
# umlgdb is a expect shell for gdb, which is specialised
# to handle re-loading symbols of kernel modules as they
# are reloaded using rmmod and insmod.
#
# It spawns GDB and prints out its pid which should be
# be used to start the UML session as follows.
#
# ./linux <kernel switches> debug gdb-pid=<pid>
#
# Once the kernel starts booting, proceed with the gdb and enter 'att 1'
# to attach to the kernel and 'c' to continue.
#
# When any module is loaded with an insmod <module> command,
# gdb breaks automatically at sys_init_module
# and executes the appropriate gdb commands to reload all the symbols
# for the kernel and the symbols for the recently laoded module.
#
# It then passes control to the user who can proceed with their debugging
# as usual.
#
# umlgdb session is very much like a session started with the normal gdb
# except for the special handling of breakpoints at sys_init_modle.
#
# Its very easy to extend the behaviour opf umlgdb to handle different
# debugging situations that need automation.
#
# Author: Chandan Kudige, April 11, 2002
#######################################################################

#######################################################################
# Customisation: most of the defaults here should suffice. But feel
#  free to change anything to suit custom requirements.
#######################################################################

source /root/uml/uml_base.sh

##
# GDB program name
##
set PROG "gdb"

##
# Kernel path relative to current directory.
##
set ARG $LINUX

##
# GDB prompt - Anyway valid TCL regex. Defaults to '(gdb)'
##
set GDB {\(gdb\)}

##
# Regex pattern printed out by the gdb when 'sys_init_module'
# breakpoint is hit.
# The name of the module is matched by the regex.
##
#Breakpoint 1, SyS_init_module
#set RE_BREAKPOINT {Breakpoint 1, SyS_init_module \(name_user=[x0-9a-f]+ "(.+)"}
set RE_BREAKPOINT {Breakpoint 1.*}

#Breakpoint 1, SyS_init_module (umod=1073917952, len=1353204, uargs=1076047752) at kernel/module.c:3352
#Breakpoint 1, SyS_init_module


##
# Special character for quitting the UML session.
# Type the actual character (using your editor's escape)
##
set QUIT_CHAR ""

###
# Module paths:
# You can add paths for modules that are not in the gdb load-path.
# This is basically a list with alternating module name and module path.
#
# When a module is loaded, umlgdb tries to load the symbols from the
# path given here. If the module is not listed then no symbols are loaded.
###
#"rdbmodule" "/root/uml/umlsender/install/lib/modules/3.12.0-rdb-module+/extra/rdbmodule.ko"
set MODULE_PATHS {
"rdbmodule" "/root/uml/umlsender/install/lib/modules/3.12.0-rdb-module+/extra/rdbmodule.ko"
}


#######################################################################
# Script starts here.
#######################################################################

log_user 0
system stty -echo raw
set timeout -1

proc get_module_path2 {modname} {
	global MODULE_PATHS ARG env UMLROOT
	set idx [lsearch $MODULE_PATHS $modname]

	if {$idx == -1} {
		set version "[exec $ARG --version]"
		set path "[exec find $UMLROOT/lib/modules/$version/ -name ${modname}.ko ]"
		if {$path != ""} {
			return $path
		}
		return 0
	}

	incr idx
	return [lindex $MODULE_PATHS $idx]
}

proc get_module_path {modname} {
    global MODULE_PATHS ARG env UMLROOT

	#puts "\nmodname: $modname\n"
    set idx [lsearch $MODULE_PATHS $modname]

    if {$idx == -1} {
	set version "[exec $ARG --version]"
	#puts "CMD: find $UMLROOT/lib/modules/$version/ -name ${modname}.o"

	set path "[exec find $UMLROOT/lib/modules/$version/ -name ${modname}.o ]"
	#puts "path: $path\n"
	if {$path != ""} {
	    return $path
	}
        return 0
    }
    incr idx
    return [lindex $MODULE_PATHS $idx]
}

proc get_uml_pid {} {
    global UMLPID
	set pid "[exec /bin/cat $UMLPID]"
	puts "get_uml_pid: $pid\n"
    return $pid
}


#######################################################################
# do_insmod() hook is invoked when umlgdb detects that the breakpoint
# at sys_init_module has been hit.
#
# Args:
#   name - name of the module being loaded
#
# Returns: None
#######################################################################

proc do_insmod {name} {
    global GDB ARG

    set timeout 5

    ##
    # Let the sys_init_module return
    # since module_list struct is initialised in this routine.
    ##
    prompt_wait $GDB
    send "finish\r"
    prompt_wait $GDB

    ##
    # find the start address of the module.
    ##
    set tmp "p/x (int)module_list + module_list->size_of_struct\r"
    send $tmp

    ##
    # gdb prints the expression values as
    # $<num> = 0x<hex value>
    # Capture the hex value into modstart
    ##
    set buf [prompt_wait {= (0x[0-9a-f]+)}]
    set succ [regexp "= (.+)" $buf tmp modstart]

    set modpath [get_module_path $name]

    ##
    # Reload symbols only if we have a module path
    ##

    if {$modpath != 0} {
        ##
        # Reload all kernel symbols
        ##
        prompt_wait $GDB
        send "symbol-file $ARG\r"
        prompt_wait "y or n"
        send "y\r"

        ##
        # Load the symbols for our module
        ##
        prompt_wait $GDB


        send "add-symbol-file $modpath $modstart\r"
        prompt_wait "y or n"
        send "y\r"

        ##
        # print the module_list head to make sure init() and cleanup()
        # are valid addresses
        ##
        prompt_wait $GDB
        send "p *module_list\r"
        prompt_wait $GDB
        puts "\r\r>> Finished loading symbols for $name ...\r"

        send " \r"
    }

    prompt_wait $GDB

    ##
    # Back to user
    ##
    set timeout -1
}

#######################################################################
# shell() is the main dispatch loop.
#
# - Passes user
#
# Args:
#   name - name of the module being loaded
#
# Returns: None
#######################################################################

proc shell {} {
    global spawn_id, user_spawn_id
    global RE_BREAKPOINT
    global QUIT_CHAR
    global GDB

    while 1 {
    	expect {
            ##
            # gracefully exit when the gdb exits.
            ##
            eof {
                return
                }

           # ##
           # # Hooks for gdb output.
           # # Note: order is important
           # ##
           # -re     $RE_BREAKPOINT {
           #         set modname $expect_out(1,string);
           #         set bpline $expect_out(buffer);
		   #
           #         puts "\r *** Module $modname loaded *** \r"
           #         send_user -raw  -- $bpline
           #         do_insmod $modname
           #    }

			##
			# Hooks for gdb output.
			# Note: order is important
			##
			-re $RE_BREAKPOINT {
				puts "Entered Breakpoint: $RE_BREAKPOINT \n"
				send "info line\n"
			    #prompt_wait $GDB
				do_insmod_module_loaded
			}


            ##
            # Catch all from gdb and pass it to the user
            ##
            -re ".+" {
                    send_user -raw -- $expect_out(buffer);
               }

            ##
            # Catch user QUIT sequence.
            # Currently should be a single character.
            ##
	    	-i $user_spawn_id $QUIT_CHAR {return}

            ##
            # Catch all from user and pass it to gdb
            ##
            -i $user_spawn_id -re .+ {
                    send -- $expect_out(buffer);
               }

        }
	}
}

#######################################################################
# do_insmod_module_loaded() hook is invoked when umlgdb_wrapper detects that the
# breakpoint at sys_init_module (set at MODULE_BREAKPOINT) has been hit.
#
# Returns: None
#######################################################################

#proc load_mod {name} {
#    global GDB ARG
#
#    prompt_wait $GDB
#
#    ##
#    # find the start address of the module.
#    ##
#	set query "p/x (int)(struct module * ) modules - (int)&((struct module *)0).list\r"
#	send $query
#	set answer [prompt_wait {= (0x[0-9a-f]+)}]
#	set succ [regexp "= (.+)" $answer query modules_list_addr ]
#
#    ##
#    # gdb prints the expression values as
#    # $<num> = 0x<hex value>
#    # Capture the hex value into modstart
#    ##
#    set buf [prompt_wait {= (0x[0-9a-f]+)}]
#    set succ [regexp "= (.+)" $buf tmp modstart]
#
#    set modpath [get_module_path $name]
#
#    ##
#    # Reload symbols only if we have a module path
#    ##
#
#    if {$modpath != 0} {
#        ##
#        # Reload all kernel symbols
#        ##
#        prompt_wait $GDB
#        send "symbol-file $ARG\r"
#        prompt_wait "y or n"
#        send "y\r"
#
#        ##
#        # Load the symbols for our module
#        ##
#        prompt_wait $GDB
#
#
#        send "add-symbol-file $modpath $modstart\r"
#        prompt_wait "y or n"
#        send "y\r"
#
#        ##
#        # print the module_list head to make sure init() and cleanup()
#        # are valid addresses
#        ##
#        prompt_wait $GDB
#        send "p *module_list\r"
#        prompt_wait $GDB
#        puts "\r\r>> Finished loading symbols for $name ...\r"
#
#        send " \r"
#    }
#
#    prompt_wait $GDB
#
#    ##
#    # Back to user
#    ##
#    set timeout -1
#}




#set RE_BREAKPOINT {Breakpoint 12.*}

proc module_loaded_debug {} {

}

proc do_insmod_module_loaded {} {
	global GDB ARG
	#puts "\n *** Loading symbols *** \n"
	prompt_wait $GDB

	send "handle SIGSEGV nostop\n"
	prompt_wait $GDB
	send "finish\n"

	# find the start address of the modules list pointer
	set query "p/x (int)(struct module * ) modules - (int)&((struct module *)0).list\n"
	send $query

	set answer [prompt_wait {= (0x[0-9a-f]+)}]
	#puts "\nanswer: $answer\n"
	set succ [regexp "= (.+)" $answer query modules_list_addr ]
	#puts "\nsucc1: $succ"
	#puts "modules_list_addr: $modules_list_addr"

	#find the module name loaded righ now at userspace
	set query "p ( (struct module *) $modules_list_addr )->name\n"
	send $query
	set answer [prompt_wait {= "(.+)", }]
	#puts "answer1: $answer"

	set succ [regexp "\"(.+)\"" $answer query module_name ]
	#puts "succ: $succ"
	#puts "answer: $answer"
	#puts "module_name: $module_name"

    #puts "modules_list_addr: $modules_list_addr"

	#$2 = "rdbmodule", '\000' <repeats 46 times>

	# find the start address that the module was loaded
	set query "p ( (struct module *) $modules_list_addr )->module_core\n"
	send $query

	set answer [prompt_wait {= \(void \*\) (0x[0-9a-f]+)}]
	set succ [regexp "=.+(0x.+)" $answer query module_core_addr ]
	#puts "succ: $succ"
	#puts "answer: $answer"
	#puts "module_core_addr: $module_core_addr"
	puts "module_name: $module_name"

	set module_path [get_module_path $module_name]
	puts "module_path: $module_path\n"
	if { $module_path != 0 } {
		# Load the symbols for our module
		prompt_wait $GDB
		send "remove-symbol-file $module_path\n"

    	expect {
            "No symbol file found" {
            }
			"y or n" { send "y\n" }
			}

		#prompt_wait "y or n"
		#send "y"
		prompt_wait $GDB
		send "add-symbol-file $module_path $module_core_addr\n"
		prompt_wait "y or n"
		send "y\n"
	}

	puts "\r *** Loaded symbols of module $module_name inserted *** \r\n"
	# Send a step and so gdb prints the next instruction: mod->init()
	# and leave in the prompt another 's'tep to be <enter>ed by the user
	send "\n"
	prompt_wait $GDB
	send "handle SIGSEGV stop\n"
	prompt_wait $GDB
	send "inf li\n"

	prompt_wait $GDB
	#send "b rdb_module.c:rdbcong_init\n"

	#send "b rdb_tfrc.c:rdb_tfrc_init\n"
	#prompt_wait $GDB
	send "c\n"

	# Back to user
	set timeout -1
}

#######################################################################
# prompt_wait() : Can be called anytime we wait for a prompt.
# Respects user input while waiting for the prompt.
#######################################################################

proc prompt_wait {prompt} {
    global user_spawn_id
    global QUIT_CHAR

    set buf "<none>"

    expect {
        -re $prompt {
                set buf $expect_out(buffer);
				#puts "Matched: $prompt, buf: $buf\n"
                send_user -raw -- $buf
            }

        -re ".+" {
                set buf $expect_out(buffer);
                send_user -raw -- $buf
                exp_continue;
            }

           timeout {
                puts "\rTIMEDOUT on prompt $prompt!!!\r"
                return 0;
            }

	    	-i $user_spawn_id $QUIT_CHAR {return}
            -i $user_spawn_id -re .+ {
                    send -- $expect_out(buffer);
            }
    }

    return $buf
}

##
# Setup a breakpoint at sys_init_module
# Prepare to enter "att 1"
##
proc initgdb {pid uml_pid} {
    global GDB

##
# user can press the 'enter' once they start the UML kernel.
##

    prompt_wait $GDB
    send "att $uml_pid\n"

    prompt_wait $GDB
    #send "b SyS_init_module\n"
	send "b sys_init_module\n"
	#send "b module.c:3320\n"
	#send "b module.c:do_init_module\n"

    prompt_wait $GDB
    send "handle SIGSEGV pass nostop\n"

    prompt_wait $GDB
    send "continue\n"

#    prompt_wait $GDB
#    send "continue\n"
#
#    prompt_wait $GDB
#    send "continue\n"
#
#    prompt_wait $GDB
#    send "continue\n"


#    prompt_wait $GDB
#    send "b panic\r"
#
#    prompt_wait $GDB
#    send "b stop\r"
#
#    prompt_wait $GDB
#    send "handle SIGWINCH nostop noprint pass\r"
#
#    prompt_wait $GDB
#    send "b start_kernel\r"
#
#    prompt_wait $GDB
#    send "c\r"
}

set uml_pid [get_uml_pid]

##
# Spawn our gdb with linux
##
set pid [spawn $PROG $ARG]

puts "\r\n\n            ******** GDB pid is $pid ********\r"
#puts "Start UML as: $ARG <kernel switches> debug gdb-pid=$pid\r\n\r\n\r\n"

puts "UML instance pid: $uml_pid"

##
# Initialise gdb
##
initgdb $pid $uml_pid

##
# Main dispatch loop
##
shell

##
# Cleanup
##
system stty sane

puts "\r----- umlgdb finished----\r"
