#!/usr/bin/expect
# Used by umlgdb.sh and umsender/run.sh

#set LINUX /root/net-next-uml/vmlinux
set LINUX /root/net-next-workdirs/net-next-rdb-4-uml/vmlinux
set MEMORY "300M"
set UMLNAME "rdbuml"
set UMLDIR "/root/uml"
set UMLROOT "/root/uml/umlsender/install"
set UMLPID "/root/.uml/rdbuml/pid"
set UMLVDE0 "/root/uml/vde/switch0"
set UMLVDE1 "/root/uml/vde/switch1"
set UMLVDE0_MAC "$UMLVDE0,de:10:48:a3:31:1a"
set UMLVDE1_MAC "$UMLVDE1,de:10:48:a3:31:1b"

set ::env(TMPDIR) "/uml_tmp"

# VDE settings
set HOST_INTERFACE "eth1"
set HOST_INTERFACE_EXT "eth0"
